
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
int main()
{

	int fd;
	char * myfifo = "/tmp/myfifo";

	/*create the FIFO (named pipe) */
	mkfifo(myfifo, 0666);
	
	/* write the FIFO (named pipe */
	fd=open(myfifo, O_WRONLY);
	write(fd, "Hi", sizeof("Hi"));
	close(fd);

	/* remove the FIFO */
	unlink(myfifo);

return 0;
}
